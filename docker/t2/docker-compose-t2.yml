version: "3.7"

########################### NETWORKS
networks:
  t2_proxy:
    external:
      name: t2_proxy
  default:
    driver: bridge

########################### SERVICES
services:
# All services / apps go below this line
# Traefik 2 - Reverse Proxy
  traefik:
    container_name: traefik
    image: traefik:chevrotin # the chevrotin tag refers to v2.2.x
    restart: unless-stopped
    command: # CLI arguments
      - --global.checkNewVersion=true
      - --global.sendAnonymousUsage=false
      - --entryPoints.http.address=:80
      - --entryPoints.https.address=:443
      # Allow these IPs to set the X-Forwarded-* headers - Cloudflare IPs: https://www.cloudflare.com/ips/
      #- --entrypoints.https.forwardedHeaders.trustedIPs=173.245.48.0/20,103.21.244.0/22,103.22.200.0/22,103.31.4.0/22,141.101.64.0/18,108.162.192.0/18,190.93.240.0/20,188.114.96.0/20,197.234.240.0/22,198.41.128.0/17,162.158.0.0/15,104.16.0.0/12,172.64.0.0/13,131.0.72.0/22
      - --entryPoints.traefik.address=:8080
      - --api=true
      #      - --api.insecure=true
      #      - --serversTransport.insecureSkipVerify=true
      - --log=true 
      - --log.level=DEBUG # (Default: error) DEBUG, INFO, WARN, ERROR, FATAL, PANIC
      - --accessLog=true
      - --accessLog.filePath=/traefik.log
      - --accessLog.bufferingSize=100 # Configuring a buffer of 100 lines
      - --accessLog.filters.statusCodes=400-499
      - --providers.docker=true
      - --providers.docker.endpoint=unix:///var/run/docker.sock
      - --providers.docker.defaultrule=Host(`{{ index .Labels "com.docker.compose.service" }}.$DOMAINNAME`)
      - --providers.docker.exposedByDefault=false
      - --providers.docker.network=t2_proxy
      - --providers.docker.swarmMode=false
      - --providers.file.directory=/rules # Load dynamic configuration from one or more .toml or .yml files in a directory.
#      - --providers.file.filename=/path/to/file # Load dynamic configuration from a file.
      - --providers.file.watch=true # Only works on top level files in the rules folder
   # - --certificatesResolvers.dns-cloudflare.acme.caServer=https://acme-staging-v02.api.letsencrypt.org/directory # LetsEncrypt Staging Server - uncomment when testing
      - --certificatesResolvers.dns-cloudflare.acme.email=$CLOUDFLARE_EMAIL
      - --certificatesResolvers.dns-cloudflare.acme.storage=/acme.json
      - --certificatesResolvers.dns-cloudflare.acme.dnsChallenge.provider=cloudflare
      - --certificatesResolvers.dns-cloudflare.acme.dnsChallenge.resolvers=1.1.1.1:53,1.0.0.1:53
    #networks:
    #    t2_proxy:
    #      ipv4_address: 192.168.90.254 # You can specify a static IP
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    ports:
      - target: 80
        published: 80
        protocol: tcp
        mode: host
      - target: 443
        published: 443
        protocol: tcp
        mode: host
      - target: 8080
        published: 8080
        protocol: tcp
        mode: host
    volumes:
      - $DOCKER_VOL_DIR/config/traefik2/rules:/rules 
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - $DOCKER_VOL_DIR/config/traefik2/acme/acme.json:/acme.json 
      - $DOCKER_VOL_DIR/config/traefik2/traefik.log:/traefik.log 
      - $DOCKER_VOL_DIR/config/shared:/shared
    environment:
      - CF_API_EMAIL=$CLOUDFLARE_EMAIL
      - CF_API_KEY=$CLOUDFLARE_API_KEY
    labels:
      - "traefik.enable=true"
      # HTTP-to-HTTPS Redirect
      - "traefik.http.routers.http-catchall.entrypoints=http"
      - "traefik.http.routers.http-catchall.rule=HostRegexp(`{host:.+}`)"
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"  
         # HTTP Routers
      - "traefik.http.routers.traefik-rtr.entrypoints=https"
      - "traefik.http.routers.traefik-rtr.rule=Host(`traefik.$DOMAINNAME`)"
      - "traefik.http.routers.traefik-rtr.tls=true"
      #- "traefik.http.routers.traefik-rtr.tls.certresolver=dns-cloudflare" # Comment out this line after first run of traefik to force the use of wildcard certs
      - "traefik.http.routers.traefik-rtr.tls.domains[0].main=$DOMAINNAME"
      - "traefik.http.routers.traefik-rtr.tls.domains[0].sans=*.$DOMAINNAME"
 #    - "traefik.http.routers.traefik-rtr.tls.domains[1].main=$SECONDDOMAINNAME" # Pulls main cert for second domain
 #    - "traefik.http.routers.traefik-rtr.tls.domains[1].sans=*.$SECONDDOMAINNAME" # Pulls wildcard cert for second domain
      ## Services - API
      - "traefik.http.routers.traefik-rtr.service=api@internal"
        ## Middlewares
      - "traefik.http.routers.traefik-rtr.middlewares=chain-basic-auth@file"

  # Organizr - Unified Frontend
  organizr:
    container_name: organizr
    image: organizrtools/organizr-v2:latest
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
  #    ports:
  #      - "$ORGANIZR_PORT:80"
    volumes:
      - $DOCKER_VOL_DIR/config/organizr:/config
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - TZ=$TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.organizr-rtr.entrypoints=https"
      - "traefik.http.routers.organizr-rtr.rule=Host(`$DOMAINNAME`,`www.$DOMAINNAME`)" 
      - "traefik.http.routers.organizr-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.organizr-rtr.middlewares=chain-basic-auth@file" 
      ## HTTP Services
      - "traefik.http.routers.organizr-rtr.service=organizr-svc"
      - "traefik.http.services.organizr-svc.loadbalancer.server.port=80"

  # Heimdall - Unified Frontend Alternative
  # Putting all services behind Oragnizr slows things down.
  heimdall:
    container_name: heimdall
    image: linuxserver/heimdall:latest
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    #ports:
    #- "$HEIMDALL_PORT:80"
    volumes:
      - $DOCKER_VOL_DIR/config/heimdall:/config
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - TZ=$TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.heimdall-rtr.entrypoints=https"
      - "traefik.http.routers.heimdall-rtr.rule=Host(`heimdall.$DOMAINNAME`)"
      - "traefik.http.routers.heimdall-rtr.tls=true"
  #      - "traefik.http.routers.heimdall-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.heimdall-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.heimdall-rtr.service=heimdall-svc"
      - "traefik.http.services.heimdall-svc.loadbalancer.server.port=80"

      # Portainer - WebUI for Containers
  portainer:
    container_name: portainer
    image: portainer/portainer:latest
    restart: unless-stopped
    command: -H unix:///var/run/docker.sock
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    ports:
      - "9000:9000"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - $DOCKER_VOL_DIR/config/portainer/data:/data 
    environment:
      - TZ=$TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.portainer-rtr.entrypoints=https"
      - "traefik.http.routers.portainer-rtr.rule=Host(`portainer.$DOMAINNAME`)"
      - "traefik.http.routers.portainer-rtr.tls=true"
      ## Middlewares
      - "traefik.http.routers.portainer-rtr.middlewares=chain-no-auth@file" # No Authentication
#      - "traefik.http.routers.portainer-rtr.middlewares=chain-basic-auth@file" # Basic Authentication
#      -  "traefik.http.routers.portainer-rtr.middlewares=chain-oauth@file" # Google OAuth 2.0
      ## HTTP Services
      - "traefik.http.routers.portainer-rtr.service=portainer-svc"
      - "traefik.http.services.portainer-svc.loadbalancer.server.port=9000"
  
# SABnzbd - Binary newsgrabber (NZB downloader)
# Disable SABNnzbd's built-in HTTPS support for traefik proxy to work
# Needs trailing / if using PathPrefix
  sabnzbd:
    image: linuxserver/sabnzbd:latest
    container_name: sabnzbd
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    ports:
      - "9999:8080"
    volumes:
      - ${DOCKER_VOL_DIR}/config/sabnzbd:/config
      - ${DOCKER_VOL_DIR}/config/downloads:/downloads
      - ${DOCKER_VOL_DIR}/config/sabnzbd/nzb:/nzb
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 002
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.sabnzbd-rtr.entrypoints=https"
      - "traefik.http.routers.sabnzbd-rtr.rule=Host(`sabnzbd.$DOMAINNAME`)"
      - "traefik.http.routers.sabnzbd-rtr.tls=true"
#      - "traefik.http.routers.sabnzbd-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.sabnzbd-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.sabnzbd-rtr.service=sabnzbd-svc"
      - "traefik.http.services.sabnzbd-svc.loadbalancer.server.port=8080"
  
  sonarr:
#    image: aront/sonarr  #for mp4_automator support
    image: linuxserver/sonarr:preview
    container_name: sonarr
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    #ports:
    #  - "8989:8989"
    volumes:
      - ${DOCKER_VOL_DIR}/config/sonarr:/config
      - ${DOCKER_VOL_DIR}/config/downloads/complete/tv:/downloads
      - ${DOCKER_VOL_DIR}/media/TV:/tv
      - ${DOCKER_VOL_DIR}/media/Wrestle TV/:/wrestletv
      - "/etc/localtime:/etc/localtime:ro"
#      - "$USERDIR/docker/shared/mp4_automator:/config_mp4_automator:rw"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.sonarr-rtr.entrypoints=https"
      - "traefik.http.routers.sonarr-rtr.rule=Host(`sonarr.$DOMAINNAME`)"
      - "traefik.http.routers.sonarr-rtr.tls=true"
#      - "traefik.http.routers.sonarr-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.sonarr-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.sonarr-rtr.service=sonarr-svc"
      - "traefik.http.services.sonarr-svc.loadbalancer.server.port=8989"

# Radarr - Movie management
# Set url_base in radarr settings if using PathPrefix
  radarr:
#    image: aront/radarr #for mp4_automator support
    image: linuxserver/radarr
    container_name: radarr
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    ports:
      - "7878:7878"
    volumes:
      - ${DOCKER_VOL_DIR}/config/radarr:/config
      - ${DOCKER_VOL_DIR}/config/downloads/complete/movies:/downloads
      - ${DOCKER_VOL_DIR}/media/Movies:/movies
      - ${DOCKER_VOL_DIR}/media/Wrestle PPV:/wrestlePPV
      - ${DOCKER_VOL_DIR}/media/Other Movies:/othermovies
#      - $USERDIR/docker/shared/mp4_automator:/config_mp4_automator
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 022
      
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.radarr-rtr.entrypoints=https"
      - "traefik.http.routers.radarr-rtr.rule=Host(`$DOMAINNAME`)"
      - "traefik.http.routers.radarr-rtr.tls=true"
#      - "traefik.http.routers.radarr-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.radarr-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.radarr-rtr.service=radarr-svc"
      - "traefik.http.services.radarr-svc.loadbalancer.server.port=7878"

# Tautulli - Previously PlexPy. Plex statistics and monitoring
# Set HTTP Root in Tautulli settings if using PathPrefix
  tautulli:
    image: linuxserver/tautulli:latest
    container_name: tautulli
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
   # ports:
   #   - "8181:8181"
    volumes:
      - ${DOCKER_VOL_DIR}/config/tautulli:/config
      - ${DOCKER_VOL_DIR}/config/plex/Library/Application Support/Plex Media Server/Logs:/logs:ro
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.tautulli-rtr.entrypoints=https"
      - "traefik.http.routers.tautulli-rtr.rule=Host(`tautulli.$DOMAINNAME`)"
      - "traefik.http.routers.tautulli-rtr.tls=true"
#      - "traefik.http.routers.tautulli-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.tautulli-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.tautulli-rtr.service=tautulli-svc"
      - "traefik.http.services.tautulli-svc.loadbalancer.server.port=8181"

# Ombi - Media Requests
  ombi:
    image: linuxserver/ombi:latest
    container_name: ombi
    restart: unless-stopped
    networks:
      - t2_proxy
    ports:
      - "3579:3579"
    security_opt:
      - no-new-privileges:true
    volumes:
      - ${DOCKER_VOL_DIR}/config/ombi:/config
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      #BASE_URL: /ombi #optional
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.ombi-rtr.entrypoints=https"
      - "traefik.http.routers.ombi-rtr.rule=Host(`ombi.$DOMAINNAME`)"
      - "traefik.http.routers.ombi-rtr.tls=true"
#      - "traefik.http.routers.ombi-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.ombi-rtr.middlewares=chain-no-auth@file"
      ## HTTP Services
      - "traefik.http.routers.ombi-rtr.service=ombi-svc"
      - "traefik.http.services.ombi-svc.loadbalancer.server.port=3579"



############################# INDEXERS

# NZBHydra2 - NZB meta search
  hydra:
    image: linuxserver/nzbhydra2:latest
    container_name: hydra
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    #ports:
    #  - "5076:5076"
    volumes:
      - $DOCKER_VOL_DIR/config/hydra:/config
      - $DOCKER_VOL_DIR/config/downloads:/downloads
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.hydra-rtr.entrypoints=https"
      - "traefik.http.routers.hydra-rtr.rule=Host(`$DOMAINNAME`) && PathPrefix(`/hydra`)"
      - "traefik.http.routers.hydra-rtr.tls=true"
  #      - "traefik.http.routers.hydra-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.hydra-rtr.middlewares=chain-no-auth@file"
      ## HTTP Services
      - "traefik.http.routers.hydra-rtr.service=hydra-svc"
      - "traefik.http.services.hydra-svc.loadbalancer.server.port=5076"
############################# MEDIA FILE MANAGEMENT

# Bazarr - Subtitle Management
  bazarr:
    image: linuxserver/bazarr:latest
    container_name: bazarr
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    ports:
      - "6767:6767"
    volumes:
      - ${DOCKER_VOL_DIR}/config/bazarr:/config
      - ${DOCKER_VOL_DIR}/media/Movies:/movies
      - ${DOCKER_VOL_DIR}/media/TV:/tv
      - ${DOCKER_VOL_DIR}/media/Wrestle PPV:/wrestlePPV
      - ${DOCKER_VOL_DIR}/media/Wrestle TV:/wrestleTV
      - ${DOCKER_VOL_DIR}/media/Other Movies:/othermovies
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 022
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.bazarr-rtr.entrypoints=https"
      - "traefik.http.routers.bazarr-rtr.rule=Host(`bazarr.$DOMAINNAME`)"
      - "traefik.http.routers.bazarr-rtr.tls=true"
#      - "traefik.http.routers.bazarr-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.bazarr-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.bazarr-rtr.service=bazarr-svc"
      - "traefik.http.services.bazarr-svc.loadbalancer.server.port=6767"

###########################  OTHER
  guacamole:
    image: oznu/guacamole:latest
    container_name: guacamole
    restart: unless-stopped
    networks:
      - t2_proxy
    security_opt:
      - no-new-privileges:true
    #ports:
    #  - "8080:8080"
    volumes:
      - $DOCKER_VOL_DIR/config/guacamole:/config
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    labels:
      - "traefik.enable=true"
      ## HTTP Routers
      - "traefik.http.routers.guac-rtr.entrypoints=https"
      - "traefik.http.routers.guac-rtr.rule=Host(`guac.$DOMAINNAME`)"
      - "traefik.http.routers.guac-rtr.tls=true"
  #      - "traefik.http.routers.guac-rtr.tls.certresolver=dns-cloudflare" 
      ## Middlewares
      - "traefik.http.routers.guac-rtr.middlewares=chain-basic-auth@file"
      ## HTTP Services
      - "traefik.http.routers.guac-rtr.service=guac-svc"
      - "traefik.http.services.guac-svc.loadbalancer.server.port=8080"




 

