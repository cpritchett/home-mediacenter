version: "3.7"

########################### NETWORKS
networks:
  t2_proxy:
    external: true
    name: t2_proxy

########################### SERVICES
services:

# Organizr - Unified Frontend
  organizr:
    image: organizrtools/organizr-v2:latest
    networks:
      - t2_proxy
  #    ports:
  #      - "$ORGANIZR_PORT:80"
    volumes:
      - $DOCKER_VOL_DIR/config/organizr:/config
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - TZ=$TZ
    deploy:
      placement:
        constraints:
          - node.role == manager  
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.organizr-rtr.entrypoints=https"
        - "traefik.http.routers.organizr-rtr.rule=Host(`$DOMAINNAME`,`www.$DOMAINNAME`)" 
        - "traefik.http.routers.organizr-rtr.tls=true"
        ## Middlewares
        - "traefik.http.routers.organizr-rtr.middlewares=chain-authelia@file" 
        ## HTTP Services
        - "traefik.http.routers.organizr-rtr.service=organizr-svc"
        - "traefik.http.services.organizr-svc.loadbalancer.server.port=80"

  # Heimdall - Unified Frontend Alternative
  # Putting all services behind Oragnizr slows things down.
  heimdall:
    image: linuxserver/heimdall:latest
    networks:
      - t2_proxy
    #ports:
    #- "$HEIMDALL_PORT:80"
    volumes:
      - $DOCKER_VOL_DIR/config/heimdall:/config
    environment:
      - PUID=$PUID
      - PGID=$PGID
      - TZ=$TZ   
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.heimdall-rtr.entrypoints=https"
        - "traefik.http.routers.heimdall-rtr.rule=Host(`heimdall.$DOMAINNAME`)"
        - "traefik.http.routers.heimdall-rtr.tls=true"
    #      - "traefik.http.routers.heimdall-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.heimdall-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.heimdall-rtr.service=heimdall-svc"
        - "traefik.http.services.heimdall-svc.loadbalancer.server.port=80"# calibre - Subtitle Management

# Disable SABNnzbd's built-in HTTPS support for traefik proxy to work
# Needs trailing / if using PathPrefix
  sabnzbd:
    image: linuxserver/sabnzbd:latest
    networks:
      - t2_proxy
    ports:
      - "9999:8080"
    volumes:
      - ${DOCKER_VOL_DIR}/config/sabnzbd:/config
      - ${DOCKER_VOL_DIR}/config/downloads:/downloads
      - ${DOCKER_VOL_DIR}/config/sabnzbd/nzb:/nzb
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 002    
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.sabnzbd-rtr.entrypoints=https"
        - "traefik.http.routers.sabnzbd-rtr.rule=Host(`sabnzbd.$DOMAINNAME`)"
        - "traefik.http.routers.sabnzbd-rtr.tls=true"
  #      - "traefik.http.routers.sabnzbd-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.sabnzbd-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.sabnzbd-rtr.service=sabnzbd-svc"
        - "traefik.http.services.sabnzbd-svc.loadbalancer.server.port=8080"
  
  sonarr:
#    image: aront/sonarr  #for mp4_automator support
    image: linuxserver/sonarr:preview
    networks:
      - t2_proxy
    #ports:
    #  - "8989:8989"
    volumes:
      - ${DOCKER_VOL_DIR}/config/sonarr:/config
      - ${DOCKER_VOL_DIR}/config/downloads/complete/tv:/downloads
      - ${DOCKER_VOL_DIR}/media/TV:/tv
      - ${DOCKER_VOL_DIR}/media/Wrestle TV/:/wrestletv
      - "/etc/localtime:/etc/localtime:ro"
#      - "$USERDIR/docker/shared/mp4_automator:/config_mp4_automator:rw"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.sonarr-rtr.entrypoints=https"
        - "traefik.http.routers.sonarr-rtr.rule=Host(`sonarr.$DOMAINNAME`)"
        - "traefik.http.routers.sonarr-rtr.tls=true"
  #      - "traefik.http.routers.sonarr-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.sonarr-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.sonarr-rtr.service=sonarr-svc"
        - "traefik.http.services.sonarr-svc.loadbalancer.server.port=8989"

# Radarr - Movie management
# Set url_base in radarr settings if using PathPrefix
  radarr:
#    image: aront/radarr #for mp4_automator support
    image: ghcr.io/linuxserver/radarr:nightly
    networks:
      - t2_proxy
    ports:
      - "7878:7878"
    volumes:
      - ${DOCKER_VOL_DIR}/config/radarr:/config
      - ${DOCKER_VOL_DIR}/config/downloads/complete/movies:/downloads
      - ${DOCKER_VOL_DIR}/media/Movies:/movies
      - ${DOCKER_VOL_DIR}/media/Wrestle PPV:/wrestlePPV
      - ${DOCKER_VOL_DIR}/media/Other Movies:/othermovies
#      - $USERDIR/docker/shared/mp4_automator:/config_mp4_automator
      - "/etc/localtime:/etc/localtime:ro"
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 022
    deploy:
      placement:
        constraints:
          - node.role == manager       
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.radarr-rtr.entrypoints=https"
        - "traefik.http.routers.radarr-rtr.rule=Host(`radarr.$DOMAINNAME`)"
        - "traefik.http.routers.radarr-rtr.tls=true"
  #      - "traefik.http.routers.radarr-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.radarr-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.radarr-rtr.service=radarr-svc"
        - "traefik.http.services.radarr-svc.loadbalancer.server.port=7878"

# Tautulli - Previously PlexPy. Plex statistics and monitoring
# Set HTTP Root in Tautulli settings if using PathPrefix
  tautulli:
    image: linuxserver/tautulli:latest
    networks:
      - t2_proxy
   # ports:
   #   - "8181:8181"
    volumes:
      - ${DOCKER_VOL_DIR}/config/tautulli:/config
      - ${DOCKER_VOL_DIR}/config/plex/Library/Application Support/Plex Media Server/Logs:/logs:ro
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.tautulli-rtr.entrypoints=https"
        - "traefik.http.routers.tautulli-rtr.rule=Host(`tautulli.$DOMAINNAME`)"
        - "traefik.http.routers.tautulli-rtr.tls=true"
  #      - "traefik.http.routers.tautulli-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.tautulli-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.tautulli-rtr.service=tautulli-svc"
        - "traefik.http.services.tautulli-svc.loadbalancer.server.port=8181"

# Ombi - Media Requests
  ombi:
    image: linuxserver/ombi:latest
    networks:
      - t2_proxy
    ports:
      - "3579:3579"
    volumes:
      - ${DOCKER_VOL_DIR}/config/ombi:/config
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      #BASE_URL: /ombi #optional
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.ombi-rtr.entrypoints=https"
        - "traefik.http.routers.ombi-rtr.rule=Host(`ombi.$DOMAINNAME`)"
        - "traefik.http.routers.ombi-rtr.tls=true"
  #      - "traefik.http.routers.ombi-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.ombi-rtr.middlewares=chain-no-auth@file"
        ## HTTP Services
        - "traefik.http.routers.ombi-rtr.service=ombi-svc"
        - "traefik.http.services.ombi-svc.loadbalancer.server.port=3579"


############################# BOOKS
### Calibre - ebook management
  calibre:
    image: linuxserver/calibre:latest
    networks:
      - t2_proxy
    #ports:
    #  - "8080:8080"
    volumes:
      - ${DOCKER_VOL_DIR}/media/Books:/config
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ    
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.calibre-rtr.entrypoints=https"
        - "traefik.http.routers.calibre-rtr.rule=Host(`calibre.$DOMAINNAME`)"
        - "traefik.http.routers.calibre-rtr.tls=true"
  #      - "traefik.http.routers.calibre-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.calibre-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.calibre-rtr.service=calibre-svc"
        - "traefik.http.services.calibre-svc.loadbalancer.server.port=8080"# SABnzbd - Binary newsgrabber (NZB downloader)

  calibre-web:
    image: linuxserver/calibre-web:latest
    networks:
      - t2_proxy
    ports:
      - "8083:8083"
    volumes:
      - ${DOCKER_VOL_DIR}/config/calibre-web:/config
      - ${DOCKER_VOL_DIR}/media/Books:/books:ro
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ    
      DOCKER_MODS: linuxserver/calibre-web:calibre
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.calibrew-rtr.entrypoints=https"
        - "traefik.http.routers.calibrew-rtr.rule=Host(`books.$DOMAINNAME`)"
        - "traefik.http.routers.calibrew-rtr.tls=true"
  #      - "traefik.http.routers.calibrew-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.calibrew-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.calibrew-rtr.service=calibrew-svc"
        - "traefik.http.services.calibrew-svc.loadbalancer.server.port=8083"


############################# MUSIC

  lidarr:
    image: linuxserver/lidarr
    environment:
      - PUID=${PUID}
      - PGID=${PGID}
      - TZ=${TZ}
      - UMASK_SET=022
    volumes:
      - ${DOCKER_VOL_DIR}/config/lidarr:/config
      - ${DOCKER_VOL_DIR}/media/Music:/music
      - ${DOCKER_VOL_DIR}/config/downloads/complete/audio:/downloads
    ports:
      - 8686:8686
    networks:
      - t2_proxy
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.calibrew-rtr.entrypoints=https"
        - "traefik.http.routers.lidarr-rtr.rule=Host(`lidarr.$DOMAINNAME`)"
        - "traefik.http.routers.lidarr-rtr.tls=true"
  #      - "traefik.http.routers.lidarr-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.lidarr-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.lidarr-rtr.service=lidarr-svc"
        - "traefik.http.services.lidarr-svc.loadbalancer.server.port=8686"

############################# INDEXERS

# NZBHydra2 - NZB meta search
  hydra:
    image: ghcr.io/linuxserver/nzbhydra2:latest
    networks:
      - t2_proxy
    #ports:
    #  - "5076:5076"
    volumes:
      - $DOCKER_VOL_DIR/config/hydra:/config
      - $DOCKER_VOL_DIR/config/downloads:/downloads
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    deploy:
      placement:
        constraints:
          - node.role == manager
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.hydra-rtr.entrypoints=https"
        - "traefik.http.routers.hydra-rtr.rule=Host(`$DOMAINNAME`) && PathPrefix(`/hydra`)"
        - "traefik.http.routers.hydra-rtr.tls=true"
    #      - "traefik.http.routers.hydra-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.hydra-rtr.middlewares=chain-no-auth@file"
        ## HTTP Services
        - "traefik.http.routers.hydra-rtr.service=hydra-svc"
        - "traefik.http.services.hydra-svc.loadbalancer.server.port=5076"

############################# MEDIA FILE MANAGEMENT

# Bazarr - Subtitle Management
  bazarr:
    image: linuxserver/bazarr:latest
    networks:
      - t2_proxy
    ports:
      - "6767:6767"
    volumes:
      - ${DOCKER_VOL_DIR}/config/bazarr:/config
      - ${DOCKER_VOL_DIR}/media/Movies:/movies
      - ${DOCKER_VOL_DIR}/media/TV:/tv
      - ${DOCKER_VOL_DIR}/media/Wrestle PPV:/wrestlePPV
      - ${DOCKER_VOL_DIR}/media/Wrestle TV:/wrestleTV
      - ${DOCKER_VOL_DIR}/media/Other Movies:/othermovies
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
      UMASK_SET: 022
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.bazarr-rtr.entrypoints=https"
        - "traefik.http.routers.bazarr-rtr.rule=Host(`bazarr.$DOMAINNAME`)"
        - "traefik.http.routers.bazarr-rtr.tls=true"
  #      - "traefik.http.routers.bazarr-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        - "traefik.http.routers.bazarr-rtr.middlewares=chain-authelia@file"
        ## HTTP Services
        - "traefik.http.routers.bazarr-rtr.service=bazarr-svc"
        - "traefik.http.services.bazarr-svc.loadbalancer.server.port=6767"

# Tautulli - Previously PlexPy. Plex statistics and monitoring
# Set HTTP Root in Tautulli settings if using PathPrefix
  tautulli:
    image: linuxserver/tautulli:latest
    networks:
      - t2_proxy
   # ports:
   #   - "8181:8181"
    volumes:
      - ${DOCKER_VOL_DIR}/config/tautulli:/config
      #- ${DOCKER_VOL_DIR}/config/plex/Library/Application Support/Plex Media Server/Logs:/logs:ro
    environment:
      PUID: $PUID
      PGID: $PGID
      TZ: $TZ
    deploy:
      placement:
        constraints:
          - node.role == manager 
      labels:
        - "traefik.enable=true"
        ## HTTP Routers
        - "traefik.http.routers.tautulli-rtr.entrypoints=https"
        - "traefik.http.routers.tautulli-rtr.rule=Host(`tautulli.$DOMAINNAME`)"
        - "traefik.http.routers.tautulli-rtr.tls=true"
    #      - "traefik.http.routers.tautulli-rtr.tls.certresolver=dns-cloudflare" 
        ## Middlewares
        #- "traefik.http.routers.tautulli-rtr.middlewares=chain-basic-auth@file"
        - "traefik.http.routers.tautulli-rtr.middlewares=chain-no-auth@file"
        ## HTTP Services
        - "traefik.http.routers.tautulli-rtr.service=tautulli-svc"
        - "traefik.http.services.tautulli-svc.loadbalancer.server.port=8181"


  
